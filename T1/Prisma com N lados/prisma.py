from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import math
import numpy as np

n = 4

cores = ( (1,0,0),(1,1,0),(0,1,0),(0,1,1),(0,0,1),(1,0,1),(0.5,1,1),(1,0,0.5) )

def poligono():
    pontos_pol_frente = []
    pontos_pol_tras = []
    raio = 0.7

    glBegin(GL_QUADS)
    z = 5
    # glVertex2f(0,0, z)
    for i in range(0,n):
        a = (i/n) * 2 * math.pi
        x = raio * math.cos(a)
        y = raio * math.sin(a)
        glVertex3f(x,y,z)
        pontos_pol_frente.append([x,y,z])
    glEnd()

    glBegin(GL_QUADS)
    z = 0
    # glVertex2f(0,0, z)
    for i in range(0,n):
        a = (i/n) * 2 * math.pi
        x = raio * math.cos(a)
        y = raio * math.sin(a)
        glVertex3f(x,y, z)
        pontos_pol_tras.append([x,y,z])
    glEnd()

    glBegin(GL_QUADS)
    vertices = pontos_pol_frente + pontos_pol_tras
    faces = []
    for i in range(0,n):
        if(i==n-1):
            faces.append([vertices[i]]) 
            faces.append([vertices[0]])
            faces.append([vertices[n]])
            faces.append([vertices[i+n]])
            break
        faces.append([vertices[i]]) 
        faces.append([vertices[i+1]])
        faces.append([vertices[i+1+n]])
        faces.append([vertices[i+n]])

    for id, f in enumerate(faces):
        # print(f)   
        glColor3fv(cores[(id+1)%len(cores)])
        glVertex3fv(f)          
    glEnd()

a = 0

def desenha():
    global a # angulo em graus
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT) #apaga a tela

    glPushMatrix() #guarda contexto
    glTranslatef(0,-2,0) #translada pra cima
    glRotatef(-a,1,1,1) # roda um pouquinho
    poligono() #desenha o polígono
    glPopMatrix() #restaura

    '''
        Essa função está relacionada ao GLUT_DOUBLE (double buffering).
        São dois buffers (como se fossem 2 papéis), que são trocados ao chamar essa função.
        Enquanto um buffer tá sendo exibido, o outro está sendo desenhado.
    '''
    glutSwapBuffers()
    a += 1

def timer(i):
    glutPostRedisplay() #Quando puder, redesenha a tela. Quando o SO decidir redesenhar a janela, chama glutDisplayFunc()
    glutTimerFunc(50,timer,1) #novamente seto para chamar a função timer daqui a 50ms


# PROGRAMA PRINCIPAL
glutInit(sys.argv)
glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE) # GLUT_MULTISAMPLE = Antialiasing
glutInitWindowSize(800,600)
glutCreateWindow("CUBO")

glutDisplayFunc(desenha) # registro de callback

glEnable(GL_MULTISAMPLE)
glEnable(GL_DEPTH_TEST)

glClearColor(0.,0.,0.,1.) # fundo preto
gluPerspective(45,800.0/600.0,0.1,100.0) #seta a transformação de perspectiva na minha janela
glTranslatef(0.0,0.0,-20) #translada -20 no eixo Z. Afasta a câmera do objeto. Empurra o desenho pra dentro da tela

glutTimerFunc(50,timer,1) #Daqui a 50ms, chama a função timer, passando "1" como parâmetro pra função
glutMainLoop() #entre no loop principal. Esse loop principal tem o manuseio de eventos



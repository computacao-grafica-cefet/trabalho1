
void setup()
{
  size(800,600);
}

void draw()
{
  background(128);
  float p0x = 100;
  float p0y = 100;
  
  float p1x = 300;
  float p1y = 600;
  
  float p2x = mouseX;
  float p2y = mouseY;
  
  float p3x = 700;
  float p3y = 100;
  
  beginShape();
  vertex(p0x, p0y);  
  for(float t = 0; t <= 1; t += 0.01){
    //A = Ponto que está na reta p0,p1
    float ax = p0x + t*(p1x-p0x); 
    float ay = p0y + t*(p1y-p0y);

    //B = Ponto que está na reta p1,p2
    float bx = p1x + t*(p2x-p1x); 
    float by = p1y + t*(p2y-p1y);

    //C = ponto que está na reta p2,p3
    float cx = p2x + t*(p3x-p2x); 
    float cy = p2y + t*(p3y-p2y);   

    //D = Ponto que está na reta AB
    float dx = ax + t*(bx-ax);
    float dy = ay + t*(by-ay);
    
    //E = Ponto que está na reta BC
    float ex = bx + t*(cx-bx);
    float ey = by + t*(cy-by);
    
    //F = ponto que está na reta DE
    float fx = dx + t*(ex-dx); 
    float fy = dy + t*(ey-dy);
    vertex(fx,fy);  
  }
  vertex(p3x, p3y);
  endShape(CLOSE);
}

Repositório criado para os trabalhos do curso de Computação Gráfica do período 2021.1.

# 1a Entrega
### Curva de Bezier de grau 3
https://bitbucket.org/computacao-grafica-cefet/trabalho1/src/master/T1/Curva%20de%20Bezier%20de%20grau%203/

### Prisma de N lados
https://bitbucket.org/computacao-grafica-cefet/trabalho1/src/master/T1/Prisma%20com%20N%20lados/

### Função implícita
https://bitbucket.org/computacao-grafica-cefet/trabalho1/src/master/T1/Fun%C3%A7%C3%A3o%20impl%C3%ADcita%20com%20malha/


# 2a Entrega
### Dado com textura
https://bitbucket.org/computacao-grafica-cefet/trabalho1/src/master/T2/dado%20com%20textura/

### Sistema Solar
https://bitbucket.org/computacao-grafica-cefet/trabalho1/src/master/T2/sistema%20solar/

### Sólidos Iluminados
https://bitbucket.org/computacao-grafica-cefet/trabalho1/src/master/T2/s%C3%B3lido%20iluminado/

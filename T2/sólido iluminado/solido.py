from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import math
import numpy as np

n = 4

cores = ((1, 0, 0), (1, 1, 0), (0, 1, 0), (0, 1, 1),
         (0, 0, 1), (1, 0, 1), (0.5, 1, 1), (1, 0, 0.5))


def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

# produto vetorial da face. Acha o vetor normal à face


def calculaNormalFace(face):
    x = 0
    y = 1
    z = 2

    v0 = face[0][0]
    v1 = face[1][0]
    v2 = face[2][0]

    U = (v2[x]-v0[x], v2[y]-v0[y], v2[z]-v0[z])
    V = (v1[x]-v0[x], v1[y]-v0[y], v1[z]-v0[z])
    N = ((U[y]*V[z]-U[z]*V[y]), (U[z]*V[x]-U[x]*V[z]), (U[x]*V[y]-U[y]*V[x]))

    # norma (tamanho) do vetor, teorema de pitágoras
    NLength = math.sqrt(N[x]*N[x]+N[y]*N[y]+N[z]*N[z])
    # retorna o vetor normalizado
    return (N[x]/NLength, N[y]/NLength, N[z]/NLength)


def poligono():
    pontos_pol_frente = []
    pontos_pol_tras = []
    raio = 0.7

    # desenha a face frontal
    glBegin(GL_QUADS)
    z = 5
    # glVertex2f(0,0, z)
    for i in range(0, n):
        a = (i/n) * 2 * math.pi
        x = raio * math.cos(a)
        y = raio * math.sin(a)
        glVertex3f(x, y, z)
        pontos_pol_frente.append([x, y, z])
    glEnd()

    # desenha a face de trás
    glBegin(GL_QUADS)
    z = 0
    # glVertex2f(0,0, z)
    for i in range(0, n):
        a = (i/n) * 2 * math.pi
        x = raio * math.cos(a)
        y = raio * math.sin(a)
        glVertex3f(x, y, z)
        pontos_pol_tras.append([x, y, z])
    glEnd()

    # seta todas as faces
    glBegin(GL_QUADS)
    vertices = pontos_pol_frente + pontos_pol_tras
    faces = []
    for i in range(0, n):
        if(i == n-1):
            faces.append([vertices[i]])
            faces.append([vertices[0]])
            faces.append([vertices[n]])
            faces.append([vertices[i+n]])
            break
        faces.append([vertices[i]])
        faces.append([vertices[i+1]])
        faces.append([vertices[i+1+n]])
        faces.append([vertices[i+n]])

    # desenha as faces laterais
    for id, f in enumerate(faces):
        # print(f)
        glColor3fv(cores[(id+1) % len(cores)])
        glVertex3fv(f)
        # glNormal3fv(calculaNormalFace(f))

    f = chunker(faces, 4)
    for face in f:
        glNormal3fv(calculaNormalFace(face))

    glEnd()


def reshape(w, h):
    glViewport(0, 0, w, h)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(w)/float(h), 0.1, 50.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(0, 1, 10, 0, 0, 0, 0, 1, 0)


a = 0


def desenha():
    global a  # angulo em graus
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # apaga a tela

    glPushMatrix()  # guarda contexto
    glTranslatef(0, -2, 0)  # translada pra cima
    glRotatef(-a, 1, 1, 1)  # roda um pouquinho
    poligono()  # desenha o polígono
    glPopMatrix()  # restaura

    '''
        Essa função está relacionada ao GLUT_DOUBLE (double buffering).
        São dois buffers (como se fossem 2 papéis), que são trocados ao chamar essa função.
        Enquanto um buffer tá sendo exibido, o outro está sendo desenhado.
    '''
    glutSwapBuffers()
    a += 1


def timer(i):
    # Quando puder, redesenha a tela. Quando o SO decidir redesenhar a janela, chama glutDisplayFunc()
    glutPostRedisplay()
    # novamente seto para chamar a função timer daqui a 50ms
    glutTimerFunc(50, timer, 1)


def init():
    mat_ambient = (0.4, 0.0, 0.0, 0.0)
    mat_diffuse = (1.0, 0.0, 0.0, 0.0)
    mat_specular = (1.0, 0.5, 0.5, 0.0)
    mat_shininess = (128,)
    light_position = (10, 0, 0)
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glShadeModel(GL_FLAT)
    # glShadeModel(GL_SMOOTH)

    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient)
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse)
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular)
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess)
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glLightfv(GL_LIGHT0, GL_POSITION, light_position)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_MULTISAMPLE)


def main():
    # PROGRAMA PRINCIPAL
    glutInit(sys.argv)
    # GLUT_MULTISAMPLE = Antialiasing
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA |
                        GLUT_DEPTH | GLUT_MULTISAMPLE)
    glutInitWindowSize(800, 600)
    glutCreateWindow("Sólido iluminado")

    glutDisplayFunc(desenha)  # registro de callback
    glutReshapeFunc(reshape)

    glEnable(GL_MULTISAMPLE)
    glEnable(GL_DEPTH_TEST)

    glClearColor(0., 0., 0., 1.)  # fundo preto
    # seta a transformação de perspectiva na minha janela
    gluPerspective(45, 800.0/600.0, 0.1, 100.0)
    # translada -20 no eixo Z. Afasta a câmera do objeto. Empurra o desenho pra dentro da tela
    glTranslatef(0.0, 0.0, -20)
    init()
    # Daqui a 50ms, chama a função timer, passando "1" como parâmetro pra função
    glutTimerFunc(50, timer, 1)
    glutMainLoop()  # entre no loop principal. Esse loop principal tem o manuseio de eventos


main()

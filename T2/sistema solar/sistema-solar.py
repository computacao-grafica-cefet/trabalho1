from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys
import time

start_time = time.time()


def delta_time():
    passed_time = time.time() - start_time
    year_period = 5.0                  # 5 seconds for simulating one year
    year = (passed_time / year_period)
    day = 365 * year
    moon_spin = (365 / 27.3) * year

    return [year, day, moon_spin]


def lua():
    glPushMatrix()

    glRotatef(delta_time()[2]*360.0, 0.0, 1.0, 0.0) # moon sidereal
    glTranslatef(1.0, 0.0, 0.0) # distance moon to earth
    glRotatef(90, 1.0, 0.0, 0.0)
    glColor4f(0.4, 0.5, 0.6, 1)
    glutSolidSphere(0.1, 10, 8) # moon
    
    glPopMatrix()


def terra():
    glRotatef(delta_time()[0]*360.0, 0.0, 1.0, 0.0) # earth rotation around the sun
    glTranslatef(3.0, 0.0, 0.0) # earth location

    glPushMatrix() # push earth system

    glRotatef(delta_time()[1]*360.0, 0.0, 1.0, 0.0) # earth spinn
    glColor3f(0, 0, 1) # blue
    glutSolidSphere(0.3, 10, 8) # earth

    glPopMatrix()


def sol():
    glPushMatrix()

    glColor4f(1.0, 1.0, 0, 1)
    glRotatef(20,0,1,0)
    glutSolidSphere(1.0, 20, 16)
    
    glPopMatrix()  # pop earth system

def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glEnable(GL_DEPTH_TEST)

    glPushMatrix()

    sol()
    terra()
    lua()

    glPopMatrix()  # pop earth system

    glutSwapBuffers()


def timer(i):
    glutPostRedisplay()
    glutTimerFunc(50, timer, 1)


def reshape(w, h):
    glViewport(0, 0, w, h)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(w)/float(h), 0.1, 50.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(0, 1, 10, 0, 0, 0, 0, 1, 0)


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA |
                        GLUT_DEPTH | GLUT_MULTISAMPLE)
    glutInitWindowSize(800, 600)
    glutCreateWindow("Esfera")
    glutReshapeFunc(reshape)
    glutDisplayFunc(display)
    glutIdleFunc(display)
    glutTimerFunc(50, timer, 1)
    glutMainLoop()


main()
